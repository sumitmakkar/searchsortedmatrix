#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

class Engine
{
private:
    vector< vector<int> > matrixVector;
    int                   elementToBeSearched;
    int matSize;
    
    void displayMatrix()
    {
        for(int i = 0 ; i < matSize ; i++)
        {
            for(int j = 0 ; j < matSize ; j++)
            {
                cout<<matrixVector[i][j]<<" ";
            }
            cout<<endl;
        }
    }
    
public:
    Engine(vector< vector<int> > mV , int len , int eTBS)
    {
        matrixVector        = mV;
        matSize             = len;
        elementToBeSearched = eTBS;
        //            displayMatrix();
    }
    
    void findAndPrintIndexOfElement()
    {
        int i = 0;
        int j = matSize - 1;
        bool elementFound = false;
        while(j >= 0 && i <= matSize - 1)
        {
            if(matrixVector[i][j] == elementToBeSearched)
            {
                cout<<"Element found at : "<<i<<" "<<j<<endl;
                elementFound = true;
                break;
            }
            else if(matrixVector[i][j] < elementToBeSearched)
            {
                i++;
            }
            else
            {
                j--;
            }
        }
        if(!elementFound)
        {
            cout<<"Element not found"<<endl;
        }
    }
};

int main(int argc, const char * argv[])
{
    int                   arr[] = {2 , 6 , 7 , 11 , 3 , 8 , 10 , 12 , 4 , 9 , 11 , 13 , 5 , 15 , 16 , 18};
    vector< vector<int> > matrixVector;
    int                   len = sqrt(sizeof(arr)/sizeof(arr[0]));
    int index = -1;
    for(int i = 0 ; i < len ; i++)
    {
        vector<int> innerArray;
        for(int j= 0 ; j < len ; j++)
        {
            innerArray.push_back(arr[++index]);
        }
        matrixVector.push_back(innerArray);
    }
    Engine e = Engine(matrixVector , len , 15);
    e.findAndPrintIndexOfElement();
    return 0;
}
